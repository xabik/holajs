'use strict'; /*jslint node:true*/

function find_object(screen, search_object){
    let array_objects = []
    for (let y = 0; y<screen.length; y++)
    {
        let row = screen[y];
        for (let x = 0; x<row.length; x++)
        {
            if (search_object.includes(row[x]))
                array_objects.push([y, x]);
        }
    }
    return array_objects;
}

function copy_screen(screen){
    let ret_screen = [];
    for (let i in screen){
        ret_screen[i] = screen[i].slice(0).split("");
    }
    return ret_screen;
}

function route_to(screen_x, departure, destination){
    //Lee algorithm
    let search = true;
    let screen = copy_screen(screen_x);
    let in_destination = false;
    let count = 2;
    let path = '';
    screen[departure[0]][departure[1]] = count;
    while(search && !in_destination){
        search = false;
        for (let y = 0; y<screen.length; y++){
            for (let x = 0; x<screen[y].length; x++){
                if (screen[y][x] == count){
                    for (let i = 0; i < 2; i++){
                        if (y + i < screen.length && x + !i < screen[y].length && ': '.includes(screen[y + i][x + !i])) {
                            screen[y + i][x + !i] = count + 1;
                            search = true;
                        }
                        if (y - i >= 0 && x - !i >= 0 && ': '.includes(screen[y - i][x - !i])) {
                            screen[y - i][x - !i] = count + 1;
                            search = true;
                        }
                        try{
                            if (y + i == destination[0] && x + !i == destination[1] ||
                                y - i == destination[0] && x - !i == destination[1]) {
                                screen[destination[0]][destination[1]] = count + 1;
                                in_destination = true;
                            }
                        }
                        catch(e){
                            return false;
                        }
                    }
                }
            }
        }
        count++;
    }
    if (in_destination){
        let y = destination[0];
        let x = destination[1];
        while (1){
            for (let i = 0; i < 2; i++){
                if (y + i < screen.length && x + !i < screen[y].length && screen[y + i][x + !i] == count - 1) {
                    x = x + !i;
                    y = y + i;
                    i ? path = path + 'u' : path = path + 'l';
                    continue;
                }
                if (y - i >=0 && x - !i >= 0 && screen[y - i][x - !i] == count - 1) {
                    x = x - !i;
                    y = y - i;
                    i ? path = path + 'd' : path = path + 'r';
                    continue;                    
                }
                if (y == departure[0] && x == departure[1]) {
                    return path.split("");
                }
            }
            count--;
        }
    }
    else return false;
}

function define_target(screen, targets, player){
    let minimum = {
        leng: 500,
        ind: 100
    }
    let curleng
    for(let i in targets){
        curleng = route_to(screen, player, targets[i]);
        if (curleng && minimum.leng > curleng.length) {
            minimum.leng = curleng.length;
            minimum.ind = i;
        }
    }
    return(route_to(screen, player, targets[minimum.ind]));
}

function check_rock(screen, y, x){
    if (y > 1 && 'O'.includes(screen[y - 2][x]) && ' '.includes(screen[y - 1][x])) return true;
    else return false;
}

function delay_and_step(landslide, moves, step){
    if (landslide){
        landslide = false;
        return step;
    }
    else{
        landslide = true;
        moves.push(step);
        return '0';
    }
}

function check_location(screen, location, lr=false){
    if (lr && 'O'.includes(screen[location[0]][location[1]]) && ' '.includes(screen[location[0]][location[1] + lr])) return true;
    else if (': *'.includes(screen[location[0]][location[1]])) return true;
    else return false;
}

function two_directs(first, second){
    if (first[1] && second[1]) {
        if (Math.round(Math.random())) return first[0];
        else return second[0];
    }
    if (first[1]) return first[0];
    if (second[1]) return second[0];
}

function direct(first, second, third){
    if (!third) {
        return two_directs(first, second);
    }
    else {
        if (first[1] && second[1] && third[1]) {
            let temp_dir = Math.random();
            if (temp_dir < 0.33) return first[0];
            else if (temp_dir > 0.66) return third[0];
            else return second[0];
        }
        else if (first[1] && second[1]) return two_directs(first, second);
        else if (first[1] && third[1]) return two_directs(first, third);
        else if (second[1] && third[1]) return two_directs(second, third);
        else if (first[1]) return first[0];
        else if (second[1]) return second[0];
        else if (third[1]) return third[0];
        else return '0';
    }
}

function check_and_step(screen, pl, moves){
    let step = moves.pop();
    let dx = 0;
    let dy = 0;
    let down = check_location(screen, [pl[0] + 1, pl[1]]);
    let left = check_location(screen, [pl[0], pl[1] - 1], -1);
    let righ = check_location(screen, [pl[0], pl[1] + 1], 1);
    (step == 'l') ? dx = -1 : ((step == 'r') ? dx = 1: ((step == 'u') ? dy = -1: dy = 1));
    if (pl[1] == 4){
        let z = 0;
    }
    if (check_rock(screen, pl[0], pl[1])){
        if (step == 'u'){
            moves = [];
            return direct(['d', down], ['l', left], ['r', righ]);
        }
        if (step == 'l' && '+O'.includes(screen[pl[0]][pl[1] - 1])){
            moves = [];
            return direct(['d', down], ['r', righ]);
        }
        if (step == 'r' && '+O'.includes(screen[pl[0]][pl[1] + 1])){
            moves = [];
            return direct(['d', down], ['l', righ]);
        }
        if (step == 'd' && '+O#'.includes(screen[pl[0] + 1][pl[1]])){
            moves = [];
            return direct(['r', down], ['l', righ]);
        }
    }
    if (check_rock(screen, pl[0] + dy, pl[1] + dx)){
        if (check_rock(screen, pl[0], pl[1])) {
            if ("lru".includes(step)) moves = [];
            switch (step){
                case 'l': {
                    return direct(['d', down], ['r', righ], false);
                }
                case 'r': {
                    return direct(['d', down], ['l', left], false);
                }
                case 'u':
                    return direct(['d', down], ['l', left], ['r', righ]);
                case 'd':
                    return step;
            }
        }
        else {
            moves = [];
            return 0;
        }
    }
    else{
        return step;
    }
}

function check_butterfly(pl, screen){
    let down = check_location(screen, [pl[0] + 1, pl[1]]);
    let left = check_location(screen, [pl[0], pl[1] - 1], -1);
    let righ = check_location(screen, [pl[0], pl[1] + 1], 1);
    let up = check_location(screen, [pl[0] - 1, pl[1]]);

    if (pl[0] > 1 && '\\|/-'.includes(screen[pl[0] - 2][pl[1] - 1])) return direct(['d', down], ['r', righ]);
    if (pl[0] > 1 && '\\|/-'.includes(screen[pl[0] - 2][pl[1] + 1])) return direct(['d', down], ['l', left]);
    if (pl[0] > 1 && '\\|/-'.includes(screen[pl[0] + 2][pl[1] - 1])) return direct(['u', up], ['r', righ]);
    if (pl[0] > 1 && '\\|/-'.includes(screen[pl[0] + 2][pl[1] + 1])) return direct(['u', up], ['l', left]);    

    if (pl[1] > 1 && '\\|/-'.includes(screen[pl[0] - 1][pl[1] - 2])) return direct(['d', down], ['r', righ]);
    if (pl[1] > 1 && '\\|/-'.includes(screen[pl[0] - 1][pl[1] + 2])) return direct(['d', down], ['l', left]);
    if (pl[1] > 1 && '\\|/-'.includes(screen[pl[0] + 1][pl[1] - 2])) return direct(['u', up], ['r', righ]);
    if (pl[1] > 1 && '\\|/-'.includes(screen[pl[0] + 1][pl[1] + 2])) return direct(['u', up], ['l', left]);    

    if (pl[0] > 1 && '\\|/-'.includes(screen[pl[0] - 2][pl[1]])) return direct(['d', down], ['l', left], ['r', righ]);
    if (pl[1] > 1 && '\\|/-'.includes(screen[pl[0]][pl[1] - 2])) return direct(['d', down], ['u', up], ['r', righ]);
    if (pl[0] < 20 && '\\|/-'.includes(screen[pl[0] + 2][pl[1]])) return direct(['l', left], ['u', up], ['r', righ]);
    if (pl[1] < 38 && '\\|/-'.includes(screen[pl[0]][pl[1] + 2])) return direct(['d', down], ['u', up], ['l', left]);

    if (pl[0] > 2 && '\\|/-'.includes(screen[pl[0] - 3][pl[1]])) return direct(['d', down], ['l', left], ['r', righ]);
    if (pl[1] > 2 && '\\|/-'.includes(screen[pl[0]][pl[1] - 3])) return direct(['d', down], ['u', up], ['r', righ]);
    if (pl[0] < 19 && '\\|/-'.includes(screen[pl[0] + 3][pl[1]])) return direct(['l', left], ['u', up], ['r', righ]);
    if (pl[1] < 37 && '\\|/-'.includes(screen[pl[0]][pl[1] + 3])) return direct(['d', down], ['u', up], ['l', left]);    
    
    if (pl[0] > 0 && '\\|/-'.includes(screen[pl[0] - 1][pl[1]])) return  direct(['d', down], ['r', righ], ['l', left]);
    if (pl[1] > 0 && '\\|/-'.includes(screen[pl[0]][pl[1] - 1])) return direct(['d', down], ['r', righ], ['u', up]);
    if (pl[0] < 21 && '\\|/-'.includes(screen[pl[0] + 1][pl[1]])) return direct(['u', up], ['r', righ], ['l', left]);
    if (pl[1] < 39 && '\\|/-'.includes(screen[pl[0]][pl[1] + 1])) return direct(['d', down], ['u', up], ['l', left]);

    if (pl[0] > 0 && pl[1] > 0 && '\\|/-'.includes(screen[pl[0] - 1][pl[1] - 1])) return direct(['d', down], ['r', righ]);
    if (pl[0] < 21 && pl[1] < 39 && '\\|/-'.includes(screen[pl[0] + 1][pl[1] + 1])) return direct(['l', left], ['u', up]);
    if (pl[0] < 21 && pl[1] > 0 && '\\|/-'.includes(screen[pl[0] + 1][pl[1] - 1])) return direct(['u', up], ['r', righ]);
    if (pl[0] > 0 && pl[1] < 39 && '\\|/-'.includes(screen[pl[0] - 1][pl[1] + 1])) return direct(['d', down], ['l', left]);

    return false;
}

exports.play = function*(screen){
    let moves = [];
    var landslide = false;
    while (true){
        let pl = find_object(screen, 'A')[0];
        let butter = check_butterfly(pl, screen);
        if (butter) {
            moves = [];
            yield butter;
        }
        else {
            if (!moves.length){
                let targets = find_object(screen, '*');
                moves = define_target(screen, targets, pl);
            }
            if (moves.length){
                yield check_and_step(screen, pl, moves);
            }
            else {
                moves = ["s"];
                yield check_and_step(screen, pl, moves);
            }
        }
    }
};
